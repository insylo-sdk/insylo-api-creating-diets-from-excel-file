# ____                        _           _    ____ ___
#/ ___|  __ _ _ __ ___  _ __ | | ___     / \  |  _ \_ _|
#\___ \ / _` | '_ ` _ \| '_ \| |/ _ \   / _ \ | |_) | |   _____
# ___) | (_| | | | | | | |_) | |  __/  / ___ \|  __/| |  |_____|
#|____/ \__,_|_| |_| |_| .__/|_|\___| /_/   \_\_|  |___|
#                      |_|
# _   _                 _                   _                 _ _      _
#| | | | _____      __ | |_ ___    ___  ___| |_    __ _    __| (_) ___| |_
#| |_| |/ _ \ \ /\ / / | __/ _ \  / __|/ _ \ __|  / _` |  / _` | |/ _ \ __|
#|  _  | (_) \ V  V /  | || (_) | \__ \  __/ |_  | (_| | | (_| | |  __/ |_
#|_| |_|\___/ \_/\_/    \__\___/  |___/\___|\__|  \__,_|  \__,_|_|\___|\__|


import pprint
import datetime
import pandas as pd
from pandas.plotting import register_matplotlib_converters
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from insyloapi import InsyloAPI

register_matplotlib_converters()

username = "username"
password = "password"
api = 'https://apis.insylo.io'

if __name__ == "__main__":    
    # Connect to the API and retrieve a new token 
    insylo = InsyloAPI(username, password, api)
    user = insylo.get_user(username)
    ownerId = user["ownerId"]
    ##############################################    
    #  ____           _
    # |  _ \ ___  ___(_)_ __   ___  ___
    # | |_) / _ \/ __| | '_ \ / _ \/ __|
    # |  _ <  __/ (__| | |_) |  __/\__ \
    # |_| \_\___|\___|_| .__/ \___||___/
    #                  |_|
    recipes = pd.read_excel('data/recipes.xlsx', sheet_name='Blad1', skiprows=0, dtype={'Code':str,'Identifier':str})
    
    for recipe in recipes.itertuples(index=True, name='Pandas'):
        code = str(getattr(recipe, "Code"))
        ptype = str(getattr(recipe,"ProductType"))	
        pgroup = str(getattr(recipe,"ProductGroup"))	
        prod = str(getattr(recipe,  "Product"))
        density = getattr(recipe,   "Density")
        identifier = str(getattr(recipe, "Identifier"))
            
        recipe = {
          "code": code,
          "density": density,
          "description": prod,
          "label": identifier,
          "ownerId": ownerId,
          "tags": [ ptype, pgroup ]
          }
        # Get available diets
        diets = insylo.post_diet(recipe)
        print(diets)
